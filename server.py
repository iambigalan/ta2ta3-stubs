from __future__ import absolute_import, division, print_function

from concurrent import futures
import grpc
import time
import sys

from core import Core
from proto import core_pb2_grpc as core_pb2_grpc

def main():
    with futures.ThreadPoolExecutor(max_workers=10) as executor:
        server_address = "0.0.0.0:45042"
        server = grpc.server(executor)
        core_pb2_grpc.add_CoreServicer_to_server(Core(), server)
        server.add_insecure_port(server_address)
        server.start()
        try:
            print('GRPC server running at %s' % server_address)
            while True:
                time.sleep(60)
        except KeyboardInterrupt:
            print('\nStopping server...')
            server.stop(0)
            print('Done.')


if __name__ == "__main__":
    main()
