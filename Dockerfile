FROM python:3

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

WORKDIR /usr/src/app
COPY requirements.txt /usr/src/app/requirements.txt
RUN pip install -r requirements.txt
COPY data /usr/src/app/data
COPY proto /usr/src/app/proto
COPY *.py /usr/src/app/

ENV D3MOUTPUTDIR=/output

EXPOSE 45042

CMD python /usr/src/app/server.py
