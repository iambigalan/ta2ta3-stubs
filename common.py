import logging

logging.basicConfig(level=logging.INFO)

logger = logging.getLogger(__name__)
__version__ = '0.1'

def print_msg(msg):
    msg = str(msg)
    for line in msg.splitlines():
        print("    | %s" % line)
    print("    \\____________________")
